Django>=4.0.0,<4.1.0
gunicorn>=20.0.0,<21.0.0
djangorestframework
django-cors-headers==3.11.0
whitenoise>=5.2.0,<6.0.0
psycopg2-binary>=2.8.0,<3.0.0
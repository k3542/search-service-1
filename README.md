# Search Service 1

[![pipeline status][pipeline-badge]][commits-gl]

## Daftar Isi

- [Daftar Isi](#daftar-isi)
- [Informasi Kelompok](#informasi-kelompok)
- [Deskripsi](#deskripsi)

## Informasi Kelompok

Penanggung Jawab Search Service 1: Rico Tadjudin

Kelompok 8 LAW-B:
- Alisha Yumna Bakri - 1906400173
- Muhammad Fathan Muthahhari - 1906293190
- Muzaki Azami - 1806205470
- Rico Tadjudin - 1906398364
- Samuel Mulatua Jeremy - 1906308305

## Deskripsi

Search Service 1 adalah instance pertama dari Search Service. Akan ada 2 buah instance search service yang identik untuk dilakukan load balancing pada servis pencarian.

Search Service sendiri adalah servis untuk melakukan pencarian akan obat-obat yang tersedia di Hospital Service.


[commits-gh]: https://github.com/k3542/pharmacy/commits/master
[pipeline-badge]: https://gitlab.com/k3542/pharmacy/badges/master/pipeline.svg
[commits-gl]: https://gitlab.com/k3542/pharmacy/-/commits/master
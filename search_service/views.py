from django.shortcuts import render
from rest_framework import mixins, generics, status
from rest_framework.response import Response

from .serializers import MedicineSearchSerializer
from .models import PharmacyServiceMedicine as Medicine


class GetListMedicineSearchView(mixins.ListModelMixin, generics.GenericAPIView):
	"""
	API to get list of medicine that is searched

	query params:
	- q (search query)
	"""
	queryset = Medicine.objects.all()
	serializer_class = MedicineSearchSerializer
	permission_classes = []

	def get_queryset(self):
		qs = super().get_queryset()
		query = self.request.GET.get('q', '')
		if query:
			return qs.filter(name__icontains=query).order_by("-pk")
		else:
			return qs.filter(name=query)

	def get(self, request, *args, **kwargs):
		return self.list(request, *args, **kwargs)
from .views import GetListMedicineSearchView
from django.urls import path

app_name = 'search1'

urlpatterns = [
    path("search", GetListMedicineSearchView.as_view()),
]
from rest_framework import serializers

from .models import PharmacyServiceMedicine as Medicine


class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)  # pragma: no cover
            existing = set(self.fields)  # pragma: no cover
            for field_name in existing - allowed:  # pragma: no cover
                self.fields.pop(field_name)


class MedicineSearchSerializer(DynamicFieldsModelSerializer):
	class Meta:
		model = Medicine
		fields = '__all__'